import gql from 'graphql-tag';

/*
query(){
    //return gqlCreator.query(this.$props.model.model,this.$props.listName);
},
variables() {
  return {where:this.where}
},
update (data) {
    return data[this.$props.listName];
},
*/

export default {
  modelToQF(model){
    let qfs = '';
    for (let field in model){

      if(model[field].reference === false || model[field].type !== 'list' && model[field].type !== 'select' && model[field].type !== 'media'){
        qfs += field + '\n'
      }else{
        qfs += field + '{';
          for(let subF in model[field].model){
            if(model[field].model[subF].type !== 'list' && model[field].model[subF].type !== 'select' && model[field].model[subF].type !== 'media'){
              qfs += subF + '\n';
            }
          }
        qfs += '}\n';
      }

    }
    return qfs;
  },
  upload(createMutation){
    return gql`mutation ($files: [Upload]!, $refId: ID!, $ref: String){
      ${createMutation}(
        refId: $refId
        ref: $ref
        files: $files
      ){
          id
          url
          name
      }
    }`
  },
  create(typeKey,mutationKey,model,queryFields){
    return gql`mutation ($item: ${typeKey}Input) {
      create${typeKey}(
        input:{
          data: $item
        }){
        ${mutationKey} {
          ${queryFields||this.modelToQF(model)}
        }
      }
    }`
  },
  query(model,listName, vars, queryFields){
    if(typeof model === 'string'){
      //count
      return gql`query($where: JSON!){
        ${listName}Connection(where: $where){
          aggregate{
            count
          }
        }
      }`;
    }else{
      //actual query
      let qSpec = vars!==undefined?vars.qSpec:'$where: JSON!';
      let qVars = vars!==undefined?vars.qVars:'where: $where';
      return gql`query(${qSpec}){
          ${listName}(${qVars}){
            ${queryFields||this.modelToQF(model)}
          }
        }`
    }
  }
};
