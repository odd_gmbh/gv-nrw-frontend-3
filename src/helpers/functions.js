export default {
  getLocalDateString(s,format){
    if(s===null)
      return null;
    let formatO = format || {
      weekday: 'short',
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    };
    let d = new Date(s);
    if(s.indexOf(".") === -1 && !isNaN(d)){
      return d.toLocaleDateString('de-DE', formatO);
    }else if(!isNaN(new Date(s.split(".")[2]+'-'+s.split(".")[1] + '-' + s.split(".")[0]))){
      return new Date(s.split(".")[2]+'-'+s.split(".")[1] + '-' + s.split(".")[0]).toLocaleDateString('de-DE', formatO);
    }else{
      return s;
    }
  },
  getDatePickerString(s,time){
    if(s === null){
      return null;
    }
    let d = new Date(s);
    if(s.indexOf(".") !== -1 && !isNaN(new Date(s.split(".")[2]+'-'+s.split(".")[1] + '-' + s.split(".")[0]))){
      d = new Date(s.split(".")[2]+'-'+s.split(".")[1] + '-' + s.split(".")[0]);
    }
    if(isNaN(d)){
      return null;
    }else{
      return time===undefined?d.toISOString().split("T")[0]:d.toISOString().split("T")[0] + " " + d.toISOString().split("T")[1].substr(0,5);
    }
  }
}
