import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import DatetimePicker from 'vuetify-datetime-picker';

Vue.use(Vuetify);
Vue.use(DatetimePicker);
export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#666666', //'#9bbfaf',
        secondary: '#44a181',
        accent: '#fc5753',//'#9bbfaf',
        error: '#ff5722',
      },
    },
  }
});
