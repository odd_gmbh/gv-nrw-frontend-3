import Vue from 'vue'
import App from './App.vue'

import VueApollo from "vue-apollo";
import apolloClient from "./vue-apollo";
import vuetify from './plugins/vuetify';
import router from "./router";


Vue.config.productionTip = false;

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
     $query: {
       fetchPolicy: 'cache-and-network'
     }
   },
   errorHandler (error) {
     //filter array issue
     console.log("apolloProvider errorHandler (src/main.js):");
     console.error(error);
   }
});

new Vue({
  apolloProvider,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
